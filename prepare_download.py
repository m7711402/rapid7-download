#!/usr/bin/env python3
"""
Download studies list from Rapid7
"""
__author__ = "Gustavo Luvizotto Cesar"
__email__ = "g.luvizottocesar@utwente.nl"

import json
import requests

import credentials as c

HEADERS = {'X-Api-Key': c.RAPID7_ACCESS_KEY}


def main():
    download_studies()


def download_studies():
    studies = "https://us.api.insight.rapid7.com/opendata/studies/"
    studies = requests.get(studies, headers=HEADERS).json()

    with open("studies.json", mode="w", encoding="utf-8") as out_f:
        json.dump(studies, out_f, indent=4)


if __name__ == '__main__':
    main()
