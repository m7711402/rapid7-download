"""
configurations for this module
"""

DATASET_DIR = "datasets"
DOWNLOAD_DIR = "download_files"
SIDEKICK_PORT = "8080"
BUF_SIZE = 65536  # read in 64kb chunks
MAX_UPLOADS = 8
MAX_UPLOAD_RETRY = 3
MAX_DOWNLOADS = 8
STUDIES = "studies.json"
DATASETS = ["sonar.fdns_v2", "sonar.http", "sonar.https", "sonar.moressl", "sonar.rdns_v2",
            "sonar.tcp", "sonar.udp", "sonar.ssl"]
