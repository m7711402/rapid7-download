#!/usr/bin/env python3
"""
Responsible to
"""
__author__ = "Gustavo Luvizotto Cesar"
__email__ = "g.luvizottocesar@utwente.nl"

from abc import ABCMeta, abstractmethod
import os


class StoragePathInterface(metaclass=ABCMeta):
    """
    asd
    """
    @abstractmethod
    def get_path(self, filename: str) -> str:
        """Load in the data set"""
        raise NotImplementedError

    @staticmethod
    def _get_year_month_day(filename: str) -> str:
        filename_list = filename.split("-")
        year = int(filename_list[0])
        month = int(filename_list[1])
        day = int(filename_list[2])
        return f"year={year:02d}/month={month:02d}/day={day:02d}"


class SonarRDNSPath(StoragePathInterface):
    def get_path(self, filename: str) -> str:
        """

        :param filename: 2022-11-09-1667952274-rdns.json.gz
        :return:
        """
        return os.path.join(super()._get_year_month_day(filename), filename)


class SonarFDNSPath(StoragePathInterface):
    def get_path(self, filename: str) -> str:
        """
        :param filename: E.g.:
            2023-02-01-1675210015-fdns_txt_mx_mta-sts.json.gz
            2023-01-28-1674873659-fdns_cname.json.gz
            2023-01-27-1674857596-fdns_mx.json.gz
            2023-01-27-1674830732-fdns_txt.json.gz
        :return:
        """
        year_month_day = super()._get_year_month_day(filename)
        record = filename.split("_")[1].split(".")[0]
        return f"record={record}/{year_month_day}/{filename}"


class SonarMoreSSLPath(StoragePathInterface):
    def get_path(self, filename: str) -> str:
        """

        :param filename: E.g.
            20221102/2022-11-02-1667389298-tcp_ldap_3268_names.gz
            20230202/2023-02-02-1675312584-pop3s_995_names.gz
            20230202/2023-02-02-1675296509-smtp_starttls_25_certs.gz
        :return:
        """
        filename_leaf = filename.split("/")[-1]
        year_month_day = super()._get_year_month_day(filename_leaf)
        cert_type = filename_leaf.split("_")[-1].split(".")[0]
        port = filename_leaf.split("_")[-2]
        protocol = filename_leaf.split("_")[0].split("-")[-1]
        if protocol == "tcp":
            protocol = filename_leaf.split("_")[-3]
        return f"type={cert_type}/protocol={protocol}/port={port}/{year_month_day}/{filename_leaf}"


class SonarSSLPath(StoragePathInterface):
    def get_path(self, filename: str) -> str:
        """
        :param filename: E.g.
            20231211/2023-12-11-1702253407-https_get_12443_certs.gz
            20231210/2023-12-10-1702178864-https_get_49592_certs.gz
        :return:
        """
        filename_leaf = filename.split("/")[-1]
        year_month_day = super()._get_year_month_day(filename_leaf)
        cert_type = filename_leaf.split("_")[-1].split(".")[0]
        port = filename_leaf.split("_")[-2]
        protocol = filename_leaf.split("_")[0].split("-")[-1]
        if protocol == "tcp":
            protocol = filename_leaf.split("_")[-3]
        return f"type={cert_type}/protocol={protocol}/port={port}/{year_month_day}/{filename_leaf}"


class SonarTCPPath(StoragePathInterface):
    """
    asd
    """
    def get_path(self, filename: str) -> str:
        """
        asd
        :param filename: E.g.:
        2023-02-02-1675312691-pop3_starttls_110.csv.gz
        2022-02-02-1643803241-ftp_21.csv.gz
        2022-02-18-1645142776-http_get_2082.csv.gz
        2022-03-02-1646216436-atg_tls250_inventory_10001.csv.gz
        2023-02-01-1675281696-tcp_bitcoin.csv.gz
        :return:
        """
        year_month_day = super()._get_year_month_day(filename)
        try:
            port = int(filename.split("_")[-1].split(".")[0])
            port_str = f"port={port}/"
        except ValueError:
            port_str = ""
        protocol = filename.split("_")[0].split("-")[-1]
        if protocol == "tcp":
            protocol = filename.split("_")[1].split(".")[0]
        return f"protocol={protocol}/{port_str}{year_month_day}/{filename}"


class SonarHTTPSPath(StoragePathInterface):
    """

    """
    def get_path(self, filename: str) -> str:
        """

        :param filename: E.g.:
            2023-02-01-1675237974-https_get_top1m.json.gz
            2022-05-16-1652663119-https_get_443.json.gz
            2022-05-09-1652104772-http_all_bigip_9443.json.gz
        :return:
        """
        year_month_day = super()._get_year_month_day(filename)
        port_or_list = filename.split("_")[-1].split(".")[0]
        try:
            port = int(port_or_list)
            port_list_str = f"port={port}/"
        except ValueError:
            port_list_str = f"list={port_or_list}/"
        protocol = filename.split("_")[0].split("-")[-1]
        return f"protocol={protocol}/{port_list_str}{year_month_day}/{filename}"


class SonarHTTPPath(StoragePathInterface):
    """
    asd
    """

    def get_path(self, filename: str) -> str:
        """

        :param filename: E.g:
        2023-02-01-1675283304-etcd_version_2379.json.gz
        2023-02-01-1675237221-http_get_top1m.json.gz
        2023-01-23-1674436005-http_get_80.json.gz
        :return:
        """
        year_month_day = super()._get_year_month_day(filename)
        port_or_list = filename.split("_")[-1].split(".")[0]
        try:
            port = int(port_or_list)
            port_list_str = f"port={port}/"
        except ValueError:
            port_list_str = f"list={port_or_list}/"
        protocol = filename.split("_")[0].split("-")[-1]
        return f"protocol={protocol}/{port_list_str}{year_month_day}/{filename}"


class SonarUDPPath(StoragePathInterface):
    """
    asd
    """
    def get_path(self, filename: str) -> str:
        """

        :param filename: E.g:
        2022-01-03-1641176463-udp_ubiquiti_discovery_10001.csv.gz
        2022-01-03-1641192083-udp_dns_53.csv.gz
        2022-01-03-1641200839-gtp-c_2123.csv.gz
        :return:
        """
        protocol = '-'.join(filename.split("_")[0].split("-")[4:])
        port = int(filename.split("_")[-1].split(".")[0])
        port_str = f"port={port}"
        year_month_day = super()._get_year_month_day(filename)
        return f"protocol={protocol}/{port_str}/{year_month_day}/{filename}"


DATASET_MAP = {
    "sonar.rdns_v2": SonarRDNSPath(),
    "sonar.fdns_v2": SonarFDNSPath(),
    "sonar.moressl": SonarMoreSSLPath(),
    "sonar.tcp": SonarTCPPath(),
    "sonar.https": SonarHTTPSPath(),
    "sonar.http": SonarHTTPPath(),
    "sonar.udp": SonarUDPPath(),
    "sonar.ssl": SonarSSLPath()
}
