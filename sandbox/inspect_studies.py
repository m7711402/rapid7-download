#!/usr/bin/env python3

import json
import requests
import os
from time import sleep


def main():
    filepath = "studies.json"
    with open(filepath, mode="r", encoding="utf-8") as json_f:
        data = json.load(json_f)

        # print(data[0].keys())
        # dict_keys(['uniqid', 'name', 'short_desc', 'long_desc', 'study_url', 'study_name',
        # 'study_venue', 'study_bibtext', 'contact_name', 'contact_email', 'organization_name',
        # 'organization_website', 'created_at', 'updated_at', 'sonarfile_set'])

        # 4 - SYN scan results for common TCP services across all of IPv4.
        syn_data = data[4]
        print('first scan: ' + syn_data['sonarfile_set'][-1] + ' last scan: ' + syn_data['sonarfile_set'][0])  # [:21]
        print(syn_data['uniqid'])

        # 5 - DNS 'ANY', 'A', 'AAAA', 'TXT', 'MX', and 'CNAME' responses for known forward DNS names.
        dns_data = data[6]
        print('first scan: ' + dns_data['sonarfile_set'][-1] + ' last scan: ' + dns_data['sonarfile_set'][0])  # [:21]
        print(dns_data['uniqid'])


# below functions are from Raffaele Sommese
def download_mx_records(access_key: str):
    headers = {'X-Api-Key': access_key}
    r = requests.get('https://us.api.insight.rapid7.com/opendata/studies/sonar.ssl/', headers=headers)
    file_list = r.json().get('sonarfile_set')
    os.makedirs("./downloads/", exist_ok=True)
    print(file_list)
#    while file_list:
#        file_name = file_list.pop()
#        download_url = get_download_url(file_name, headers)

#        if download_url:
#            r = requests.get(download_url)
#            with open('./downloads/'+file_name, 'wb') as f:
#                f.write(r.content)


def check_quota(headers):
    r = requests.get("https://us.api.insight.rapid7.com/opendata/quota/", headers=headers).json()
    if int(r.get('quota_left')) < 1:
        sleep(r.get('oldest_action_expires_in'))


def get_url(name, headers):
    check_quota(headers)

    url_string = "https://us.api.insight.rapid7.com/opendata/studies/sonar.ssl/{}/download/".format(name)
    r = requests.get(url_string, headers=headers)
    status = r.status_code
    response = r.json()
    return status, response.get('url')


def get_download_url(name, headers):
    status, url = get_url(name, headers)
    while int(status) != 200:
        print("Status code not 200, retrying...")
        status, url = get_url(name, headers)

    return url


if __name__ == '__main__':
    main()
