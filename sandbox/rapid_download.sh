#!/bin/bash

# https://opendata.rapid7.com/apihelp/

DATASET="sonar.moressl"
FILENAME="20221102/2022-11-02-1667389298-tcp_ldap_3268_names.gz"

function curl_rapid_api() {
    retval=$(curl -H "X-Api-Key: ${RAPID7_API_SECRET}" "$1") &>/dev/null
    #echo $RESPONSE
}

function show_quota() {
    curl_rapid_api "https://us.api.insight.rapid7.com/opendata/quota/";
    echo "$retval" | jq '.'
}

function create_list_of_studies() {
    curl_rapid_api "https://us.api.insight.rapid7.com/opendata/studies/" > studies.json
}

function print_file_information() {
    curl_rapid_api "https://us.api.insight.rapid7.com/opendata/studies/$1/$2/"
    echo "$retval" | jq '.'
}

function download_file() {
    curl_rapid_api "https://us.api.insight.rapid7.com/opendata/studies/$1/$2/download/"
    echo "$retval" | jq '.url'
    URL=$(echo "$retval" | jq '.url')
    echo ""
    echo "$URL"
    wget "$URL" -P data
}

#create_list_of_studies

#show_quota

#print_file_information ${DATASET} ${FILENAME}

#download_file ${DATASET} ${FILENAME}

show_quota
