#!/usr/bin/env python3
"""
Upload all files from download_files dir to objstore
"""

__author__ = "Gustavo Luvizotto Cesar"
__email__ = "g.luvizottocesar@utwente.nl"

import argparse
import concurrent.futures
from glob import glob
import os

from storage_path import DATASET_MAP
from objstore import ObjStore

from config import DOWNLOAD_DIR, MAX_UPLOADS, MAX_UPLOAD_RETRY


def main(args):
    """
    asd
    """
    upload(args.dataset)


def upload(dataset: str) -> None:
    rapid7_objstore = ObjStore("rapid7")
    attempt = 0
    while attempt < MAX_UPLOAD_RETRY:
        with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_UPLOADS) as executor:
            executor.map(_uploader_callback, _generate_files(rapid7_objstore, dataset))
        if all_files_uploaded(dataset):
            break
    assert attempt < MAX_UPLOAD_RETRY, "Not all files have been uploaded. Please solve any " \
                                       "underlying problem and run this script again."


def _generate_files(rapid7_objstore, dataset):
    for localfile in glob(os.path.join(DOWNLOAD_DIR, dataset, "*.gz")):
        yield localfile, rapid7_objstore, dataset


def _uploader_callback(localfile_rapid7_objstore_dataset):
    localfile, rapid7_objstore, dataset = localfile_rapid7_objstore_dataset
    # localfile = 'download_files/sonar.rdns_v2/2022-11-09-1667952274-rdns.json.gz'
    filename = localfile.split("/")[-1]
    target_file = _get_remote_path(dataset, filename)
    if rapid7_objstore.is_file_already_uploaded(target_file):
        print(f"File {target_file} already in objstore. Skipping...")
    else:
        _ = rapid7_objstore.upload(localfile, target_file)
        print(f"Uploaded {localfile} to {target_file}")


def _get_remote_path(dataset, filename):
    # dataset = "sonar.tcp"
    # filename = '2022-11-09-1667952274-rdns.json.gz'
    path = DATASET_MAP[dataset].get_path(filename)
    return f"dataset={dataset}/format=raw/{path}"


def all_files_uploaded(dataset: str) -> bool:
    all_uploaded = True
    rapid7_objstore = ObjStore("rapid7")
    downloaded_files = glob(os.path.join(DOWNLOAD_DIR, dataset, "*.gz"))
    actual_nr_files = 0
    for localfile in downloaded_files:
        filename = localfile.split("/")[-1]
        target_file = _get_remote_path(dataset, filename)
        actual_nr_files += rapid7_objstore.get_nr_files(target_file)

    expected_nr_files = len(downloaded_files)
    all_uploaded &= actual_nr_files == expected_nr_files
    return all_uploaded


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", required=True, type=str,
                        help="Dataset name. Either sonar.tcp, sonar.rdns_v2, sonar.fdns_v2, "
                             "sonar.moressl, sonar.https or sonar.http")
    main(parser.parse_args())
