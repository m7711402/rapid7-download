# Rapid7 download

## Considerations
This data has been requested for the CATRIN project and can only be used for purpose.  

In case you need this data, please let Ralph Holz know via r.holz@utwente.nl.  

## Description
Download Rapid7.

## Installation
Required software:  
* Podman (NOT READY YET)
* Python3

## Usage
Run the setup script to install all the python3 virtual environment dependencies.
```shell
./setup.sh
```

To run the script, you must have a ```config.py``` python file under the toplevel of this project.
This file must contain ```RAPID7_ACCESS_KEY``` with your Rapid7 access key.
Afterwards, just run the following to find out the options to properly execute the script:

```shell
venv/bin/python3 rapid7_download.py -h
```

Example to download HTTP from a specific day:
```shell
venv/bin/python3 rapid7_download.py --dataset=sonar.tcp --date=20221225 --protocol=http
```

Or to download the whole month:
```shell
venv/bin/python3 rapid7_download.py --dataset=sonar.tcp --date=202212 --protocol=http
```

## Support
In case you need support in the downloads, please contact Gustavo.  

## Roadmap 
[ ] Automate weekly download

## Contributing
Please feel free to create a branch and make a pull request to improve the automation and documentation.  

## Authors and acknowledgment
Contributors: Gustavo Luvizotto Cesar.  

## License
MIT license.  

## Project status
In progress 
