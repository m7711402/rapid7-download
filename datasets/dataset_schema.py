#!/usr/bin/env python3
"""
Extract download files per dataset

candidate file to be removed
"""
__author__ = "Gustavo Luvizotto Cesar"
__email__ = "g.luvizottocesar@utwente.nl"

from datetime import datetime
from collections import defaultdict
import json
import os
import requests

import credentials as c

DATASETS_FILE = "studies.json"
HEADERS = {'X-Api-Key': c.RAPID7_ACCESS_KEY}


def main():
    print_studies(DATASETS_FILE)
    check_quota()
    #services_dataset_dict = get_services_per_dataset(DATASETS_FILE)
    #download_sample_services(services_dataset_dict)
    #generate_schema_from_files(x_files)


def print_studies(datasets_file):
    with open(datasets_file, mode="r", encoding="utf-8") as json_f:
        datasets = json.load(json_f)
        print("Nr of studies: " + str(len(datasets)))
        i = 1
        for dataset in datasets:
            print(str(i) + " - " + dataset["short_desc"] + ". Nr of files: " +
                  str(len(dataset["sonarfile_set"])) + " " + str(dataset['uniqid']))
            i += 1


def get_services_per_dataset(datasets_file):
    with open(datasets_file, mode="r", encoding="utf-8") as json_f:
        datasets = json.load(json_f)
        services_per_dataset_dict = defaultdict(dict)
        for dataset in datasets:
            for snapshot in dataset['sonarfile_set']:  # snapshot: '2022-11-09-1667952274-rdns.json.gz'
                year = snapshot[:4]  # year: 2022
                if datetime.strptime(year, '%Y') < datetime.strptime("2018", '%Y'):
                    # services only above this are taken into account
                    break
                service = snapshot.split("-")[-1].split(".")[0]  # service: 'rdns'
                services_per_dataset_dict[dataset["uniqid"]].update({service: snapshot})
    return services_per_dataset_dict


def download_sample_services(services_per_dataset_dict):
    if not check_quota():
        return
    for dataset, services_dict in services_per_dataset_dict.items():
        i = 0
        if not os.path.exists(dataset):
            os.mkdir(dataset)
        for filename in services_dict.values():
            output_filename = os.path.join(dataset, filename)
            if os.path.exists(output_filename):
                print(output_filename + " exists, skipping...")
                continue
            download = "https://us.api.insight.rapid7.com/opendata/studies/{}/{}/download/"\
                        .format(dataset, filename)
            url = requests.get(download, headers=HEADERS).json().get("url")
            r = requests.get(url)
            with open(output_filename, mode="wb") as out_f:
                out_f.write(r.content)
                i += 1
            del r
            if i > 1:
                break


def check_quota():
    r = requests.get("https://us.api.insight.rapid7.com/opendata/quota/",
                     headers=HEADERS).json()
    if int(r.get("quota_left")) < 1:
        print(str(r.get('oldest_action_expires_in')))
        return False
    print(str(r))
    return True


if __name__ == '__main__':
    main()
