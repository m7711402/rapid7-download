#!/usr/bin/env python3
"""
Unit test for storage_path module
"""
__author__ = "Gustavo Luvizotto Cesar"
__email__ = "g.luvizottocesar@utwente.nl"

import unittest
from parameterized import parameterized

from storage_path import DATASET_MAP


class TestSequence(unittest.TestCase):
    """
    asd
    """
    def _test_get_path(self, dataset, input_sut, expected):
        # Arrange
        sut = DATASET_MAP[dataset]  # map of sut

        # Act
        actual = sut.get_path(input_sut)

        # Assert
        self.assertEqual(expected, actual)

    @parameterized.expand([
        # name, input_sut, expected
        ("default", "2022-11-09-1667952274-rdns.json.gz", "year=2022/month=11/day=09/2022-11-09-1667952274-rdns.json.gz"),
    ])
    def test_sonar_rdns(self, name, input_sut, expected):
        self._test_get_path("sonar.rdns_v2", input_sut, expected)

    @parameterized.expand([
        # name, input_sut, expected
        ("multiple_records", "2023-02-01-1675210015-fdns_txt_mx_mta-sts.json.gz", "record=txt/year=2023/month=02/day=01/2023-02-01-1675210015-fdns_txt_mx_mta-sts.json.gz"),
        ("cname_records", "2023-01-28-1674873659-fdns_cname.json.gz", "record=cname/year=2023/month=01/day=28/2023-01-28-1674873659-fdns_cname.json.gz"),
        ("mx_record", "2023-01-27-1674857596-fdns_mx.json.gz", "record=mx/year=2023/month=01/day=27/2023-01-27-1674857596-fdns_mx.json.gz"),
        ("txt_record", "2023-01-27-1674830732-fdns_txt.json.gz", "record=txt/year=2023/month=01/day=27/2023-01-27-1674830732-fdns_txt.json.gz"),
    ])
    def test_sonar_fdns(self, name, input_sut, expected):
        self._test_get_path("sonar.fdns_v2", input_sut, expected)

    @parameterized.expand([
        # name, input_sut, expected
        ("with_tcp", "20221102/2022-11-02-1667389298-tcp_ldap_3268_names.gz", "type=names/protocol=ldap/port=3268/year=2022/month=11/day=02/2022-11-02-1667389298-tcp_ldap_3268_names.gz"),
        ("default", "20230202/2023-02-02-1675312584-pop3s_995_names.gz", "type=names/protocol=pop3s/port=995/year=2023/month=02/day=02/2023-02-02-1675312584-pop3s_995_names.gz"),
        ("with_starttls", "20230202/2023-02-02-1675296509-smtp_starttls_25_certs.gz", "type=certs/protocol=smtp/port=25/year=2023/month=02/day=02/2023-02-02-1675296509-smtp_starttls_25_certs.gz"),
    ])
    def test_sonar_moressl(self, name, input_sut, expected):
        self._test_get_path("sonar.moressl", input_sut, expected)

    @parameterized.expand([
        # name, input_sut, expected
        ("default", "20231210/2023-12-10-1702178864-https_get_49592_certs.gz", "type=certs/protocol=https/port=49592/year=2023/month=12/day=10/2023-12-10-1702178864-https_get_49592_certs.gz"),
        ])
    def test_sonar_ssl(self, name, input_sut, expected):
        self._test_get_path("sonar.ssl", input_sut, expected)

    @parameterized.expand([
        # name, input_sut, expected
        ("with_starttls", "2023-02-02-1675312691-pop3_starttls_110.csv.gz", "protocol=pop3/port=110/year=2023/month=02/day=02/2023-02-02-1675312691-pop3_starttls_110.csv.gz"),
        ("default", "2022-02-02-1643803241-ftp_21.csv.gz", "protocol=ftp/port=21/year=2022/month=02/day=02/2022-02-02-1643803241-ftp_21.csv.gz"),
        ("http_get", "2022-02-18-1645142776-http_get_2082.csv.gz", "protocol=http/port=2082/year=2022/month=02/day=18/2022-02-18-1645142776-http_get_2082.csv.gz"),
        ("multiple", "2022-03-02-1646216436-atg_tls250_inventory_10001.csv.gz", "protocol=atg/port=10001/year=2022/month=03/day=02/2022-03-02-1646216436-atg_tls250_inventory_10001.csv.gz"),
        ("bitcoin", "2023-02-01-1675281696-tcp_bitcoin.csv.gz", "protocol=bitcoin/year=2023/month=02/day=01/2023-02-01-1675281696-tcp_bitcoin.csv.gz"),
    ])
    def test_sonar_tcp(self, name, input_sut, expected):
        self._test_get_path("sonar.tcp", input_sut, expected)

    @parameterized.expand([
        # name, input_sut, expected
        ("top1m", "2023-02-01-1675237974-https_get_top1m.json.gz", "protocol=https/list=top1m/year=2023/month=02/day=01/2023-02-01-1675237974-https_get_top1m.json.gz"),
        ("default", "2022-05-16-1652663119-https_get_443.json.gz", "protocol=https/port=443/year=2022/month=05/day=16/2022-05-16-1652663119-https_get_443.json.gz"),
        ("multiple", "2022-05-09-1652104772-http_all_bigip_9443.json.gz", "protocol=http/port=9443/year=2022/month=05/day=09/2022-05-09-1652104772-http_all_bigip_9443.json.gz"),
    ])
    def test_sonar_https(self, name, input_sut, expected):
        self._test_get_path("sonar.https", input_sut, expected)

    @parameterized.expand([
        # name, input_sut, expected
        ("top1m", "2023-02-01-1675237221-http_get_top1m.json.gz", "protocol=http/list=top1m/year=2023/month=02/day=01/2023-02-01-1675237221-http_get_top1m.json.gz"),
        ("etcd", "2023-02-01-1675283304-etcd_version_2379.json.gz", "protocol=etcd/port=2379/year=2023/month=02/day=01/2023-02-01-1675283304-etcd_version_2379.json.gz"),
        ("default", "2023-01-23-1674436005-http_get_80.json.gz", "protocol=http/port=80/year=2023/month=01/day=23/2023-01-23-1674436005-http_get_80.json.gz"),
    ])
    def test_sonar_http(self, name, input_sut, expected):
        self._test_get_path("sonar.http", input_sut, expected)

    @parameterized.expand([
        # name, input_sut, expected
        ("udp4", "2022-01-03-1641176463-udp_ubiquiti_discovery_10001.csv.gz", "protocol=udp/port=10001/year=2022/month=01/day=03/2022-01-03-1641176463-udp_ubiquiti_discovery_10001.csv.gz"),
        ("default", "2022-01-03-1641192083-udp_dns_53.csv.gz", "protocol=udp/port=53/year=2022/month=01/day=03/2022-01-03-1641192083-udp_dns_53.csv.gz"),
        ("gtp", "2022-01-03-1641200839-gtp-c_2123.csv.gz", "protocol=gtp-c/port=2123/year=2022/month=01/day=03/2022-01-03-1641200839-gtp-c_2123.csv.gz"),
    ])
    def test_sonar_udp(self, name, input_sut, expected):
        self._test_get_path("sonar.udp", input_sut, expected)


if __name__ == '__main__':
    unittest.main()
