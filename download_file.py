#!/usr/bin/env python3
"""
Download snapshots from Rapid7
"""
__author__ = "Gustavo Luvizotto Cesar"
__email__ = "g.luvizottocesar@utwente.nl"

import argparse
import concurrent.futures
import sys
from datetime import datetime, timedelta
import hashlib
import json
import os

import requests

from storage_path import DATASET_MAP
from objstore import ObjStore
from config import MAX_DOWNLOADS, STUDIES, DOWNLOAD_DIR
import credentials as c

HEADERS = {'X-Api-Key': c.RAPID7_ACCESS_KEY}


def main(args):
    """
    Trigger Rapid7 downloads
    :param args: follows the argparser
    :return: None
    """
    if args.print_quota:
        print_quota()

    if args.date is not None and args.dataset is not None and args.protocol is not None:
        is_whole_month = False
        try:
            # download single snapshot
            date = datetime.strptime(args.date, "%Y%m%d")
        except ValueError:
            # download whole month
            date = datetime.strptime(args.date, "%Y%m")
            is_whole_month = True

        snapshots = _get_files(args.dataset, date, args.protocol, is_whole_month)
        do_download(snapshots, args.dataset)
    elif args.filename is not None and args.dataset is not None:
        download_from_file(args.dataset, args.filename)


def print_quota():
    r = requests.get("https://us.api.insight.rapid7.com/opendata/quota/",
                     headers=HEADERS).json()
    print(str(r))


def _get_files(dataset, date, protocol, is_whole_month):
    """

    :param dataset: e.g. sonar.tcp (see datasets dir)
    :param date: the date to be downloaded
    :param protocol: the protocol to be downloaded
    :param is_whole_month: True to download the whole month of data given the date and protocol for
                           a dataset. False otherwise for a single day of download.
    :return:
    """
    download_files_list = []
    with open(STUDIES, mode="r", encoding="utf-8") as studies_f:
        study_dict = json.load(studies_f)
        for study in study_dict:
            if study["uniqid"] == dataset:
                for snapshot in study['sonarfile_set']:  # snapshot: '2022-11-09-1667952274-rdns.json.gz'
                    year = snapshot[:4]  # year: 2022
                    month = snapshot[5:7]
                    day = snapshot[8:10]
                    within_date = str(date.year) == year and str(date.month) == month
                    if not is_whole_month:
                        within_date &= str(date.day) == day
                    # FIXME below is not true for all datasets
                    protocol_matcher = '-' + protocol + '_'
                    is_desired_protocol = protocol_matcher in snapshot
                    if within_date and is_desired_protocol:
                        download_files_list.append(snapshot)

    return download_files_list


def do_download(snapshots, dataset):
    """
    Download snapshots for a date of a protocol from a rapid7 dataset
    :param snapshots: list of files to download
    :param dataset: e.g. sonar.tcp (see datasets dir)
    :return: None
    """
    with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_DOWNLOADS) as executor:
        executor.map(_downloader_callback, _generate_snapshot_dataset(snapshots, dataset))


def _generate_snapshot_dataset(snapshots, dataset):
    for snapshot in snapshots:
        yield snapshot, dataset


def _downloader_callback(snapshot_dataset):
    """
    Callback to download a file from a dataset
    :param snapshot_dataset: tuple with snapshot filename and the dataset
    :return: None
    """
    snapshot, dataset = snapshot_dataset

    remote_path = _get_remote_path(dataset, snapshot)
    rapid7_objstore = ObjStore("rapid7")
    if rapid7_objstore.is_file_already_uploaded(remote_path):
        print(f"File {snapshot} has been uploaded to ObjStore already. Skipping the download...")
        return

    if os.path.exists(os.path.join(DOWNLOAD_DIR, dataset, snapshot)):
        print(f"File {snapshot} has been downloaded already, but it has not been uploaded to "
              f"ObjStore yet. Skipping the download...")
        return

    if not _has_quota():
        return

    _download_checksum(dataset, snapshot)
    _download_snapshot(dataset, snapshot)
    _assert_checksum(dataset, snapshot)


def _get_remote_path(dataset, filename):
    # dataset = "sonar.tcp"
    # filename = '2022-11-09-1667952274-rdns.json.gz'
    path = DATASET_MAP[dataset].get_path(filename)
    return f"dataset={dataset}/format=raw/{path}"


def _has_quota():
    r = requests.get("https://us.api.insight.rapid7.com/opendata/quota/",
                     headers=HEADERS).json()
    if int(r.get("quota_left")) < 1:
        t = timedelta(seconds=r.get('oldest_action_expires_in'))
        print("You must wait. Oldest action expires in " + str(t))
        return False
    return True


def _download_snapshot(dataset: str, snapshot: str) -> None:
    download = f"https://us.api.insight.rapid7.com/opendata/studies/{dataset}/{snapshot}/download/"
    print(f"Downloading {download}")
    download_json = requests.get(download, headers=HEADERS, timeout=None).json()
    url = download_json.get("url")
    response = requests.get(url)
    output_path = os.path.join(DOWNLOAD_DIR, dataset, snapshot.split("/")[-1])
    with open(output_path, mode="wb") as out_f:
        out_f.write(response.content)
        print(f"Download completed. File is at {output_path}")


def _download_checksum(dataset: str, snapshot: str) -> None:
    checksum_url = f"https://us.api.insight.rapid7.com/opendata/studies/{dataset}/{snapshot}/"
    checksum = requests.get(checksum_url, headers=HEADERS).json().get("fingerprint")
    output_path = os.path.join(DOWNLOAD_DIR, dataset, snapshot.split("/")[-1] + ".sha1")
    with open(output_path, mode="w", encoding="utf-8") as out_f:
        out_f.write(checksum)
        print(f"Checksum downloaded. File is at {output_path}")


def _assert_checksum(dataset, snapshot):
    # https://stackoverflow.com/questions/22058048/hashing-a-file-in-python
    buff_size = 65536
    sha1 = hashlib.sha1()

    filepath = os.path.join(DOWNLOAD_DIR, dataset, snapshot)
    with open(filepath, mode="rb") as f:
        while True:
            data = f.read(buff_size)
            if not data:
                break
            sha1.update(data)
    with open(filepath + ".sha1", mode="r", encoding="utf-8") as f:
        checksum = f.read()
        if checksum != str(sha1.hexdigest()):
            print(f"ERROR! File {snapshot} is corrupted. "
                  f"Actual {sha1.hexdigest()} != expected {checksum}. Deleting.",
                  file=sys.stderr)
            os.remove(filepath)
            os.remove(filepath + ".sha1")


def download_from_file(dataset: str, filename: str):
    snapshots = _get_files_from_filename(filename)
    do_download(snapshots, dataset)


def _get_files_from_filename(filename):
    snapshots = []
    with open(filename, mode="r", encoding="utf-8") as f:
        for line in f.readlines():
            snapshots.append(line.strip())
    return snapshots


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("--dataset", required=False, type=str,
                        help="Dataset name. E.g. sonar.tcp")
    parser.add_argument("--date", required=False, type=str,
                        help="Day or month to download snapshots. Format YYYYMM[DD]. E.g. 202212 "
                             "(a.k.a. December 2022) to download the whole month or 20221210 to "
                             "download from a specific day.")
    parser.add_argument("--protocol", required=False, type=str,
                        help="Protocol to download. E.g. http.")

    parser.add_argument("--print-quota", required=False, action='store_true',
                        help="Check current quota")

    parser.add_argument("--filename", required=False,
                        help="Input file to be downloaded")

    main(parser.parse_args())
