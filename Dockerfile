# syntax=docker/dockerfile:1
# Create a self contained runner to donwload rapid7 data
# Usage: see README.md

FROM docker.io/python:3.10
LABEL authors="Gustavo Luvizotto Cesar"
LABEL email="g.luvizottocesar@utwente.nl"

ENV HOME "/app"
WORKDIR "/app"

RUN mkdir -p /app/download_files/sonar.fdns_v2
RUN mkdir -p /app/download_files/sonar.http
RUN mkdir -p /app/download_files/sonar.https
RUN mkdir -p /app/download_files/sonar.moressl
RUN mkdir -p /app/download_files/sonar.rdns_v2
RUN mkdir -p /app/download_files/sonar.tcp
RUN mkdir -p /app/download_files/sonar.udp

COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY config.py /app
COPY credentials.py /app
COPY download_file.py /app
COPY objstore.py /app
COPY prepare_download.py /app
COPY rapid7_downloader.py /app
COPY storage_path.py /app
COPY upload_file.py /app
COPY utils.py /app

ENTRYPOINT ["/app/rapid7_downloader.py"]
