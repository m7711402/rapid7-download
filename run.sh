#!/usr/bin/env bash

DATASET_DIR="datasets"

podman run --network=host -v "$(pwd)/${DATASET_DIR}:/app/${DATASET_DIR}" --rm rapid7_downloader
