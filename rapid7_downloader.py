#!/usr/bin/env python3
"""
asd
"""
__author__ = "Gustavo Luvizotto Cesar"
__email__ = "g.luvizottocesar@utwente.nl"

import argparse
from glob import glob
import os

from download_file import download_from_file
from upload_file import upload

from config import DATASET_DIR, DATASETS


def main():
    for dataset in DATASETS:
        files = glob(os.path.join(DATASET_DIR, dataset, "*.txt"))
        for filename in sorted(files):
            download_from_file(dataset, filename)
        upload(dataset)


if __name__ == '__main__':
    main()
